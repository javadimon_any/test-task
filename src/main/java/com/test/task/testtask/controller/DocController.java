package com.test.task.testtask.controller;

import com.test.task.testtask.rest.request.DocRequest;
import com.test.task.testtask.rest.response.DocResponse;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Validated
public class DocController {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public DocResponse validExceptionHandler(MethodArgumentNotValidException ex){
        DocResponse docResponse = new DocResponse();
        docResponse.setSuccess(false);

        BindingResult results = ex.getBindingResult();
        StringBuilder msg = new StringBuilder();
        for (FieldError e: results.getFieldErrors()) {
            msg.append(e.getDefaultMessage()).append("->").append(e.getRejectedValue()).append(" ");
        }

        docResponse.setMsg(msg.toString());
        return docResponse;
    }

    @PostMapping("/doc")
    @ResponseBody
    public DocResponse validateDoc(@RequestBody @Valid DocRequest docRequest){
        DocResponse docResponse = new DocResponse();
        docResponse.setSuccess(true);
        docResponse.setMsg("Request is valid");
        return docResponse;
    }
}
