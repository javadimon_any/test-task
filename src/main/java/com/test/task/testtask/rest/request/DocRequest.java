package com.test.task.testtask.rest.request;

import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class DocRequest extends Validation {
    @NotNull(message = "Seller field is mandatory")
    @Size(min = 9, max = 9)
    private String seller;

    @NotNull(message = "Customer field is mandatory")
    @Size(min = 9, max = 9)
    private String customer;

    @Valid
    @NotNull(message = "Products field is mandatory")
    private List<Product> products;

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "DocRequest{" +
                "seller='" + seller + '\'' +
                ", customer='" + customer + '\'' +
                ", products=" + products +
                '}';
    }
}

/*
{
"seller":"123534251",
"customer":"648563524",
"products":[{"name":"milk","code":"2364758363546"},{"name":"water","code":"3656352437590"}]
}
 */
