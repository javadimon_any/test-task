package com.test.task.testtask.rest.response;

public class DocResponse {

    private boolean success;
    private String msg = "";

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "DocResponse{" +
                "success=" + success +
                ", msg='" + msg + '\'' +
                '}';
    }
}
