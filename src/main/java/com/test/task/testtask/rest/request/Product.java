package com.test.task.testtask.rest.request;

import javax.validation.Validation;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Product extends Validation {

    @NotNull(message = "Name field is mandatory")
    private String name;

    @NotNull(message = "Code field is mandatory")
    @Size(min = 13, max = 13)
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
