package com.test.task.testtask.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest

public class DocControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void docTest() throws Exception{
        assertThat(mockMvc).isNotNull();

        String jsonBody = "{\n" +
                "\"seller\":\"123534251\",\n" +
                "\"customer\":\"648563524\",\n" +
                "\"products\":[{\"name\":\"milk\",\"code\":\"2364758363546\"},{\"name\":\"water\",\"code\":\"3656352437590\"}]\n" +
                "}";

        String sResponse = mockMvc.perform(post("/doc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody)).andReturn().getResponse().getContentAsString();

        System.out.println(sResponse);
        assertThat(sResponse).contains("true");

        jsonBody = "{\n" +
                "\"seller\":\"123534251\",\n" +
                "\"customer\":\"648563524\",\n" +
                "\"products\":[{\"name\":\"milk\",\"code\":\"2364758363\"},{\"name\":\"water\",\"code\":\"3656352437590\"}]\n" +
                "}";

        sResponse = mockMvc.perform(post("/doc")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonBody)).andReturn().getResponse().getContentAsString();

        System.out.println(sResponse);
        assertThat(sResponse).contains("false");
    }
}
